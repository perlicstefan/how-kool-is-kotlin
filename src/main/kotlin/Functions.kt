fun main(args: Array<String>) {
    val result = 5.add(3)
    println(result)
}

fun addTwoNumbers(firstNumber: Int, secondNumber: Int): Int {
    return firstNumber + secondNumber
}

fun Int.add(number: Int): Int {
    return this + number
}

