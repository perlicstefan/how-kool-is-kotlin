data class User(val id: Int, var firstName: String, val lastName: String, val age: Int)

fun main(args: Array<String>) {
    val user = User(1, "Stefan", "Perlic", 25)
    user.firstName = ""
    val javaUser: JavaUser? = JavaUser(2, "Don", "Johnson", 45)

    println("${javaUser?.firstName} ${javaUser?.lastName}")
    thisIsTopLevelFunction("you can invoke me anywhere")
    println(5 multiply 3)

    exponentiation(exponent = 3, base = 10)

    val listOfNumbers = 1..100000
    listOfNumbers.filter { it % 5 == 0 }
        .take(30)
        .forEach { println(it) }

    getToken {

    }
}

infix fun Int.multiply(multiplicator: Int) = this * multiplicator

fun exponentiation(base: Int, exponent: Int = 2) {
    println(Math.pow(base.db, exponent.db))
}

fun getToken(callback: (String) -> Unit): Unit {
    val token = "some token retrieved async"
    callback(token)
}

private val Int.db: Double
    get() {return this.toDouble()}

